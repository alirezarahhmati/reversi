// OTHELLO BY Alireza & KIMIA


#include <iostream>
#include <fstream>
#include <conio.h>
#include <math.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string>
#include <iomanip>
#include <ios>
#include <stdlib.h>

using namespace std;

struct game
{
    string  name_player1 , name_player2;
    int len;
    int **arr;
    bool turn = true;
    int size_ondoArray = 0;
    int *arr_undo;
    int row = 0;
    int col = 0;
};

// start of our app with main menu
int main_menu ();
void output_mainMenu (int);

// making profile to start the  game
void newGame ();
void single_player ();
void two_player ();
void loadGame ();
void output_newGame( int );

// table and game
void mainGame (game*);
void loadGame ();
void check_for_posibleTable ( game * , int *arr );
void put (game* , int , int , int *arr , int );
bool checkPut (game*, int , int );
void save ( game* );
void undo (game* );
void output_mainGame ( game *onGame , int possibleArray[] , int rowC , int colC );
void output_wrong_len(int) ;

// ranking 
void ranking ();
void save_ranking ( string , int );
inline bool exists_test0 (const std::string& name);

void hard_game ();
void easy_game ();
void move ( game *onGame , bool & );
int eror_wrong_length ();


void output_singlePlayer( int );
void output_ranking ( string , int );
void output_endGame ( game * );

//////////////////////////////////////////////////////// 
int main ()
{
    while ( true )
    {
        int curser_mainMenu = main_menu();
        switch ( curser_mainMenu)
        {
        case 0 :
            newGame ();
            break;
        case 1 :
            loadGame ();
            break;
        case 2 :
            ranking ();
            break;
        case 3 :
			system ("cls");
            return 0;
            break;
        }
    }
}


//////////////////////////////////////////////////////////
int main_menu ()
{
    int curser = 0;
    char key;
    while (true) 
    {
        system ("cls");
        output_mainMenu (curser);
        key = getch ();
        switch (key)
        {
        case '\r':
            return curser;
            break;
        case 'w':
            if (curser == 0){
                curser = 3;
            }else{
                curser--;
            }
            break;
        case 's':
            if (curser == 3){
                curser = 0;
            }else{
                curser++;
            }
            break;
        }

    }
}

void output_mainMenu ( int x )
{
    string distance = " ";
    for (int i = 0; i < 98; i++)
    {
        distance += " ";
    }

    //1
	cout << setw(20) ;
    cout << "\u250F";
    for (int i = 0; i < 99; i++)
    {
        cout << "\u2501";
    }
    cout <<  "\u2513" << endl;
    
    //2
	cout << setw(20) ;
    cout << "\u2503" << distance << "\u2503" << endl;

    //3
	cout << setw(20) ;
    cout << "\u2503" << "     " ;
    string space = "  ";
    //output for line 3 ////////////////////////////////////////
        //output o //
    cout << "\u2554";
    for (int i = 0; i < 9; i++)
    {
        cout << "\u2550";
    }
    cout << "\u2557";
        ///////////
    cout << space;

        //output T //
    for (int i = 0; i < 5; i++)
    {
        cout << "\u2550";
    }
    cout << "\u2566";
    for (int i = 0; i < 5; i++)
    {
        cout << "\u2550";
    }
        //////////
    cout << space;

        //output H ///
    cout << "\u2551" << "         " << "\u2551";
        /////////
    cout << space;

        //output E //
    cout << "\u2554";
    for (int i = 0; i < 10; i++){
        cout << "\u2550";
    }
        ////////////
    cout << space;

        //output L //
    cout << "\u2551" << "          ";
        /////////
    cout << space;
        //output L //
    cout << "\u2551" << "          ";
        /////////
    cout << space;

        //output o //
    cout << "\u2554";
    for (int i = 0; i < 9; i++)
    {
        cout << "\u2550";
    }
    cout << "\u2557";
        ///////////
    cout << "     " << "\u2503" << endl;
    //output of line 4 ////////////////////////////////////////////////
	cout << setw(20) ;
    cout << "\u2503" << "     " ;

        //output O
    cout << "\u2551" << "         " << "\u2551";
        ////////
    cout << space;
        //output T
    cout << "     " << "\u2551" << "     ";
        /////////
    cout << space;
        //output H
    cout << "\u2551" << "         " << "\u2551";
        ////////
    cout << space;
        //output E
    cout << "\u2551" << "          ";
        ///////
    cout << space;
        //ouput L
    cout << "\u2551" << "          ";
        //////
    cout << space;
        //ouput L
    cout << "\u2551" << "          ";
        //////
    cout << space;
        //ouput O
    cout << "\u2551" << "         " << "\u2551";
        ////////
    cout << "     " << "\u2503" << endl;

    //ouput of line 5 /////////////////////////////////////////////////
	cout << setw(20) ;
        cout << "\u2503" << "     " ;

        //output O
    cout << "\u2551" << "         " << "\u2551";
        ////////
    cout << space;
        //output T
    cout << "     " << "\u2551" << "     ";
        /////////
    cout << space;
        //ouput H
    cout << "\u2560";
    for (int i = 0; i < 9; i++)
    {
        cout << "\u2550";
    }
    cout << "\u2563";
        /////////
    cout << space;
        //ouput E
    cout << "\u2560";
    for (int i = 0; i < 10; i++)
    {
        cout << "\u2550";
    }
        ////////
    cout << space;
        //ouput L
    cout << "\u2551" << "          ";
        //////
    cout << space;
        //ouput L
    cout << "\u2551" << "          ";
        //////
    cout << space;
            //ouput O
    cout << "\u2551" << "         " << "\u2551";
        ////////
    cout << "     " << "\u2503" << endl;
    
    //output of line 6 /////////////////////////////////////////////
	cout << setw(20) ;
        cout << "\u2503" << "     " ;

            //output O
    cout << "\u2551" << "         " << "\u2551";
        ////////
    cout << space;
        //output T
    cout << "     " << "\u2551" << "     ";
        /////////
    cout << space;
        //output H
    cout << "\u2551" << "         " << "\u2551";
        ////////
    cout << space;
        //output E
    cout << "\u2551" << "          ";
        ///////
    cout << space;
        //ouput L
    cout << "\u2551" << "          ";
        //////
    cout << space;
        //ouput L
    cout << "\u2551" << "          ";
        //////
    cout << space;
        //ouput O
    cout << "\u2551" << "         " << "\u2551";
        ////////
    cout << "     " << "\u2503" << endl;

    //output of line 7 /////////////////////////////////////////////
	cout << setw(20) ;
        cout << "\u2503" << "     " ;

        //output O
    cout << "\u255A";
    for (int i = 0; i < 9; i++)
    {
        cout << "\u2550";
    }
    cout << "\u255D";
        ///////////
    cout << space;
        //output T //
    cout << "     " << "\u2551" << "     ";
        /////////
    cout << space;
        //ouput H ///
    cout << "\u2551" << "         " << "\u2551";
        ////////
    cout << space;
        //ouput E
    cout << "\u255A";
    for (int i = 0; i < 10; i++)
    {
        cout << "\u2550";
    }
        ///////
    cout << space;
        //ouput L
    cout << "\u255A";
    for (int i = 0; i < 10; i++)
    {
        cout << "\u2550";
    }
        ///////
    cout << space;
        //ouput L//
    cout << "\u255A";
    for (int i = 0; i < 10; i++)
    {
        cout << "\u2550";
    }
        /////////
    cout << space;
        //ouput O
    cout << "\u255A";
    for (int i = 0; i < 9; i++)
    {
        cout << "\u2550";
    }
    cout << "\u255D";
        ///////////
    cout << "     " << "\u2503" << endl;

    //8
    //cout << "\u2503" << distance << "\u2503" << endl;

    //9
	cout << setw(20) ;
    cout << "\u255a";
    for (int i = 0; i < 99; i++)
    {
        cout << "\u2501";
    }
    cout <<  "\u255d" << endl;
	cout << endl << endl ;


// main menue table

	int m = 30 ;
	int l = 17 ;
	string menue[l][m] ;
	for (int i = 0 ; i < l ; i++)
	{
		for (int j = 0 ; j < m ; j++)
		{
			menue[i][j] = " " ;
		}
	}
	//amoodi
	
	menue[0][0] = "\u2554" ; menue[0][m-1] = "\u2557" ; menue[l-1][0] = "\u255a" ; menue[l-1][m-1] = "\u255d" ;
	//amoodi
	for (int i = 1 ; i < l-1 ; i++ )
	{
		if (i%4 == 0 )
		{
			menue[i][0] = "\u2560" ;
			menue[i][m-1] = "\u2563" ;
			for (int j = 1 ; j < m-1 ; j++ )
			{
				menue[i][j] = "\u2550" ;
			}
		}
		else
		{
			menue[i][0] = "\u2551" ;
			menue[i][m-1] = "\u2551" ;
		}
	}
	// ofoghi
	for (int j = 1 ; j < m-1 ; j++)
	{
		menue[0][j] = "\u2550" ;
		menue[l-1][j] = "\u2550" ;
	}
	// new game
	menue[2][10] = "N";menue[2][11] = "e" ;menue[2][12] = "w";
	menue[2][14] = "G" ; menue[2][15] = "a" ; menue[2][16] = "m" ; menue[2][17] = "e" ;
	
	// load game
	menue[6][10] = "L" ;menue[6][11] = "o" ;menue[6][12] = "a"; menue[6][13] = "d" ;
	menue[6][15] = "G" ; menue[6][16] = "a" ; menue[6][17] = "m" ; menue[6][18] = "e" ;
	
	//ranking
	menue[10][11] = "R" ; menue[10][12] = "a" ; menue[10][13] = "n" ; menue[10][14] = "k" ; menue[10][15] = "i" ;menue[10][16] = "n" ;menue[10][17] = "g" ;
	
	// Exit
	
	menue[14][13] = "E" ; menue[14][14] = "x" ; menue[14][15] = "i" ; menue[14][16] = "t" ;
	
	//cursor //****to function bayad input begire
	//x : input
	//int x = 1  ; //****
	x = x*4 + 2 ;
	if ( x == 2)
	{
		menue[x][4] = "\u2588" ;
	}
	else if (x == 6)
	{
		menue[x][4] = "\u2588" ;
		menue[x][5] = "\u2588" ;
	}
	else if (x == 10)
	{
		menue[x][4] = "\u2588" ;
		menue[x][5] = "\u2588" ;
		menue[x][6] = "\u2588" ;
	}
	else
	{
		menue[x][4] = "\u2588" ;
		menue[x][5] = "\u2588" ;
		menue[x][6] = "\u2588" ;
		menue[x][7] = "\u2588" ;
	}
	
// prints


	for (int i = 0 ; i < l ; i++)
	{
		cout <<setw(20)<< "\u2502" << setw(35) ;
		for (int j = 0 ; j < m ; j++)
		{
			cout << menue[i][j] ;
		}
		
		cout <<setw(40) << "\u2502" << endl;
	}
	string line1 ;
	for(int i = 0 ; i < 10 ; i++)
	{
		line1 += "\u2500" ;
	}
	
	string line2 ;
	for (int i = 0 ; i < 26 ; i++ )
	{
		line2 += "\u2500" ;
	}
	
	//line 1
	cout << setw(20) << "\u2502" << "\u256d" << line2 << "\u256e" << setw(49)<< "\u256d" << line1 << "\u256e" <<setw(16)<< "\u2502"<< endl ;
	//line 2
	cout << setw(20) << "\u2502" << "\u2502" << "OTHEllO By Alireza & Kimia" << "\u2502" << setw(49) << "\u2502" << "   W(up)  " << "\u2502" <<setw(16) << "\u2502"<< endl ;
	//line 3
	cout << setw(20) << "\u2502" << "\u2570" << line2 << "\u256f" << setw(38)<< "\u256d" << line1 << "\u253c" << line1 << "\u253c" << line1 << "\u256e" << "  " << "\u2502" << endl ; 
	//line 4 
	cout << setw(20) << "\u2502" << setw(66)<< "\u2502" << " A(left)  " << "\u2502" << "  S(down) " << "\u2502" << " D(right) " << "\u2502" << "  " << "\u2502" << endl ;
	//line 5
	cout <<  setw(20) << "\u2502" << setw(66)<< "\u2570" << line1 << "\u2534" << line1 << "\u2534" << line1 << "\u256f" << "  " << "\u2502" << endl ;
	

}

/////////////////////////////////////////////////////////
void newGame ( )
{
    int curser = 0;
    char key; 
    while (true) 
    {
        system ("cls");
        output_newGame (curser);
        key = getch ();
        switch (key)
        {
        case '\r':
            if (curser == 0) {
            single_player ();
            }else {
                two_player ();
            }
            return;
            break;
        case 's':
            if (curser == 0){
                curser = 1;
            }else{
                curser--;
            }
            break;
        case 'w':
            if (curser == 1){
                curser = 0;
            }else{
                curser++;
            }
            break;
        }
    }
}


void two_player () 
{
	string space ;
	for (int i = 0 ; i < 30 ; i++ )
	{
		space += " " ;
	}
     cout << space << "Please Enter names of player1 and Player2 and Table Dimension : ";
    //read 
	cout << endl << endl ;
	/// structure 
    game *onGame = new game;

	cout << space << "          " << "\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588" ;
	cout <<endl << setw(43) <<"\u2588" <<" Player1 : " ; 
    cin >> onGame->name_player1;
	cout << endl << setw(43) << "\u2588"<< " Player 2 : " ;
	
    cin >> onGame->name_player2;
	cout << endl << setw(43) << "\u2588" << " Dimension : " ;
    cin >> onGame->len;
	cout << endl ;

    //check for invalid len for table
    if ((onGame->len < 8 ) || ( onGame->len > 14 ) || ( onGame->len % 2 != 0 ))
	{
        //output_of_eror_for_invalid_len ;
		cout << space << "          " << "\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588\u2588" << endl ;
		
		
		int curser = 0 ; 
		char key ;
		bool ans = true;
        while ( ans ) 
        {
			system ("cls") ;
			cout << space << "          " << "\u2588" " Invalid length! " << endl ;
			cout << space << "          " << "Please Choose what to do next : " << endl ;
			output_wrong_len(curser) ;
			key = getch ();
            switch (key)
            {
            case '\r':
                ans = false;
                break;
            case 'a':
                if (curser == 0){
                    curser = 1;
                }else{
                    curser--;
                }
                break;
            case 'd':
                if (curser == 1){
                    curser = 0;
            }
			else
			{  
                    curser++;
                }
                break;
            }
		}
		if (curser == 0)
		{
			cout << endl << setw(43) << "\u2588" << " Dimension : " ;
			cin >> onGame->len;
				 if ((onGame->len < 8 ) || ( onGame->len > 14 ) || ( onGame->len % 2 != 0 ))
				 {
					 return ;
				 }
			}
            else if (onGame->len == -1)
			{
                return;
            }
			else 
			{
            return;
			}
	}
  

    //making array for game and play with table
    onGame->arr = new int *[onGame->len];
    for (int i = 0; i < onGame->len; i++ )
    {
        onGame->arr[i] = new int [onGame->len];
    }

    for (int i = 0; i < onGame->len; i++)
    {
        for (int j = 0; j < onGame->len; j++)
        {
            onGame->arr[i][j] = -1;
        }
    }

    //put x and o in first of the game
    int x = onGame->len / 2 - 1;
    onGame->arr[x][x] = 1;
    onGame->arr[x + 1][x + 1] = 1;
    onGame->arr[x][x + 1] = 0;
    onGame->arr[x + 1][x] = 0;

    onGame->arr_undo = new int [ onGame->len * onGame->len ];
    mainGame (onGame);
}

void mainGame (game *onGame) 
{
    //output mainGame
    bool check_end_game = true;
    int row = 0 , col = 0;
    char key;

    while ( true )
    {

        int *arr_posibleTable = new int  [onGame->len * onGame->len];
        check_for_posibleTable ( onGame , arr_posibleTable );
        int size = arr_posibleTable[0];
        system ("cls");
        output_mainGame ( onGame , arr_posibleTable , row , col);            
        if (size == 1) {
            onGame->turn = !onGame->turn;
            check_for_posibleTable ( onGame , arr_posibleTable );
            size = arr_posibleTable[0];
            if ( size == 1 ){
                break;
            }
        }
        key = getch ();
        switch (key)
        {
        case '\r':
            put ( onGame , row , col , arr_posibleTable , size );
            break;
        case 'w':
            if (row == 0){
            row = onGame->len - 1;
            }else{
                row--;
            }
            break;
        case 's':
            if (row == onGame->len - 1){
                row = 0;
            }else{
                row++;
            }
            break;
        case 'a' :
            if (col == 0){
                col = onGame->len - 1;
            }else{
                col--;
            }
            break;
        case 'd' :
            if (col == onGame->len - 1){
                col = 0;
            }else{
                col++;
            }
            break;
        case 'v':
            save (onGame);
            break;
        case 'u':
            undo (onGame);
            break;
        case 'e' :
            return;
            break;
        }
        delete[] arr_posibleTable;
    }
	output_endGame ( onGame );
}

void check_for_posibleTable ( game *onGame  , int *arr )
{
    int t = 1;
    for (int i = 0 ; i < onGame->len ; i++)
    {
        for (int j = 0 ; j < onGame->len ; j++)
        {
            if ( onGame->arr[i][j] == -1 ) {
                if ( checkPut (onGame , i , j )) {
                    arr[t++] = i;
                    arr[t++] = j;
                }
            }
        }
    }
    arr[0] = t;
}

bool checkPut (game *onGame , int row , int col )
{
    int turn = onGame->turn;

    // check to reverse up
    for ( int i = row - 1 ; i >= 0 ; i-- )
    {
        if ( onGame->arr[i][col] == -1 ) {
            break ;
        }
        if ( onGame->arr[i][col] == turn ) {
            if ( abs (i - row) != 1 ) {
                return true;
            }
            else {
                break;
            }
        }
    }
    // check to reverse down
    for ( int i = row + 1 ; i < onGame->len ; i++ )
    {
        if ( onGame->arr[i][col] == -1 ) {
            break;
        }
        if ( onGame->arr[i][col] == turn ) {
            if ( abs (i - row) != 1 ) {
                return true;
            }
            else {
                break;
            }
        }
    }
    // check to reverse right
    for ( int i = col + 1 ; i < onGame->len ; i++ )
    {
        if ( onGame->arr[row][i] == -1 ) {
            break;
        }
        if ( onGame->arr[row][i] == turn ) {
            if ( abs (i - col) != 1 ) {
                return true;
            }
            else {
                break;
            }
        }
    }
    // check to reverse left
    for ( int i = col - 1 ; i >= 0 ; i-- )
    {
        if ( onGame->arr[row][i] == -1 ) {
            break;
        }
        if ( onGame->arr[row][i] == turn ) {
            if ( abs (i - col) != 1 ) {
                return true;
            }
            else {
                break;
            }
        }
    }
    bool stop = true;
    // check to reverse top and right
    int j = col + 1;
    for ( int i = row - 1 ; i >= 0 && j < onGame->len ; i-- )
    {
            if ( onGame->arr[i][j] == -1 ) {
                stop = false;
                break;
            }
            if ( onGame->arr[i][j] == turn ) {
                if ( abs (i - row) != 1 &&  abs (j - col) != 1) {
                    return true;
                }
                else {
                    stop = false;
                    break;
                }
            }
            j++;
    }
    // check to reverse top and left
    j = col - 1;
    for ( int i = row - 1 ; i >= 0 && j >= 0 ; i-- )
    {
            if ( onGame->arr[i][j] == -1 ) {
                stop = false;
                break;
            }
            if ( onGame->arr[i][j] == turn ) {
                if ( abs (i - row) != 1 &&  abs (j - col) != 1) {
                    return true;
                }
                else {
                    stop = false;
                    break;
                }
            }
            j--;
    }
    // check to reverse down and left
    j = col - 1;
    for ( int i = row + 1 ; i < onGame->len && j >= 0 ; i++ )
    {
            if ( onGame->arr[i][j] == -1 ) {
                stop = false;
                break;
            }
            if ( onGame->arr[i][j] == turn ) {
                if ( abs (i - row) != 1 &&  abs (j - col) != 1) {
                    return true;
                }
                else {
                    stop = false;
                    break;
                }
            }
            j--;
    }
    // check to reverse down and right
    j = col + 1;
    for ( int i = row + 1 ; i < onGame->len && j < onGame->len ; i++ )
    {
            if ( onGame->arr[i][j] == -1 ) {
                stop = false;
                break;
            }
            if ( onGame->arr[i][j] == turn ) {
                if ( abs (i - row) != 1 &&  abs (j - col) != 1) {
                    return true;
                }
                else {
                    stop = false;
                    break;
                }
            }
            j++;
    }
    return false;
}

void put (game* onGame , int row , int col ,  int *arr , int size )
{

    // check that it can reserve or not
    bool posible = false;
    for (int i = 2 ; i < size ; i += 2 )
    {
        if (arr[i] == col) {
            if ( arr[i-1] == row ) {
                posible = true;
            }
        }
    }
    if ( ! posible ) {
        return;
    }

    delete[] onGame->arr_undo;
    onGame->arr_undo = new int [ onGame->len * onGame->len];
    
    int turn = onGame->turn;
    onGame->turn = !onGame->turn;
    onGame->size_ondoArray = 0;
    // check to reverse up

    for ( int i = row - 1 ; i >= 0 ; i-- )
    {
        if ( onGame->arr[i][col] == -1 ) {
            break ;
        }
        if ( onGame->arr[i][col] == turn ) {
            if ( abs (i - row) != 1 ) {
                for (int j = row ; j > i ; j--) {
                    onGame->arr[j][col] = turn;
                    onGame->arr_undo[onGame->size_ondoArray++] = j;
                    onGame->arr_undo[onGame->size_ondoArray++] = col;
                }
                break;
            }
            else {
                break;
            }
        }
    }
    // check to reverse down
    for ( int i = row + 1 ; i < onGame->len ; i++ )
    {
        if ( onGame->arr[i][col] == -1 ) {
            break;
        }
        if ( onGame->arr[i][col] == turn ) {
            if ( abs (i - row) != 1 ) {
                for (int j = row ; j < i ; j++) {
                    onGame->arr[j][col] = turn;
                    onGame->arr_undo [ onGame->size_ondoArray++ ] = j;
                    onGame->arr_undo [ onGame->size_ondoArray++ ] = col;
                }
                break;
            }
            else {
                break;
            }
        }
    }
    // check to reverse right
    for ( int i = col + 1 ; i < onGame->len ; i++ )
    {
        if ( onGame->arr[row][i] == -1 ) {
            break;
        }
        if ( onGame->arr[row][i] == turn ) {
            if (abs (i - col) != 1 ) {
                for (int j = col ; j < i ; j++) {
                    onGame->arr[row][j] = turn;
                    onGame->arr_undo [ onGame->size_ondoArray++ ] = row;
                    onGame->arr_undo [ onGame->size_ondoArray++ ] = j;
                }
                break;
            }
            else {
                break;
            }
        }
    }
    // check to reverse left
    for ( int i = col - 1 ; i >= 0 ; i-- )
    {
        if ( onGame->arr[row][i] == -1 ) {
            break;
        }
        if ( onGame->arr[row][i] == turn ) {
            if ( abs (i - col) != 1 ) {
                for (int j = col ; j > i ; j--) {
                    onGame->arr[row][j] = turn;
                    onGame->arr_undo [ onGame->size_ondoArray++ ] = row;
                    onGame->arr_undo [ onGame->size_ondoArray++ ] = j;
                }
                break;
            }
            else {
                break;
            }
        }
    }
    // check to reverse top and right
    int j = col + 1 ;
    for ( int i = row - 1 ; i >= 0 && j < onGame->len ; i-- )
    {
            if ( onGame->arr[i][j] == -1 ) {
                break;
            }
            if ( onGame->arr[i][j] == turn ) {
                if ( abs (i - row) != 1 ) {
                    int x = row;
                        for (int y = col ; y < j && x > i; ) {
                            onGame->arr[x--][y++] = turn;
                            onGame->arr_undo [ onGame->size_ondoArray++ ] = x + 1;
                            onGame->arr_undo [ onGame->size_ondoArray++ ] = y - 1;
                        }
                    break;
                }
                else {
                    break;
                }
            }
        j++;
    }
    // check to reverse top and left
    j = col - 1 ;
    for ( int i = row - 1 ; i >= 0 && j >= 0; i-- )
    {
            if ( onGame->arr[i][j] == -1 ) {
                break;
            }
            if ( onGame->arr[i][j] == turn ) {
                if ( abs (i - row) != 0 ) {
                    int x = row;
                        for (int y = col ; y > j && x > i; ) {
                            onGame->arr[x--][y--] = turn;
                            onGame->arr_undo [ onGame->size_ondoArray++ ] = x + 1;
                            onGame->arr_undo [ onGame->size_ondoArray++ ] = y + 1;
                        }
                    break;
                }
                else {
                    break;
                }
            }
        j--;
    }
    // check to reverse down and left
    j = col - 1 ;
    for ( int i = row + 1 ; i < onGame->len && j >= 0; i++ )
    {
            if ( onGame->arr[i][j] == -1 ) {
                break;
            }
            if ( onGame->arr[i][j] == turn ) {
                if ( abs (i - row) != 0 ) {
                    int x = row;
                        for (int y = col ; y > j && x < i;  ) {
                            onGame->arr[x++][y--] = turn;
                            onGame->arr_undo [ onGame->size_ondoArray++ ] = x - 1;
                            onGame->arr_undo [ onGame->size_ondoArray++ ] = y + 1;
                        }
                    break;
                }
                else {
                    break;
                }
            }
        j--;
    }
    // check to reverse down and right
    j = col + 1 ;
    for ( int i = row + 1 ; i < onGame->len && j < onGame->len; i++ )
    {
            if ( onGame->arr[i][j] == -1 ) {
                break;
            }
            if ( onGame->arr[i][j] == turn ) {
                if ( abs (i - row) != 0 ) {
                    int x = row;
                        for (int y = col ; y < j && x < i; ) {
                            onGame->arr[x++][y++] = turn;
                            onGame->arr_undo [ onGame->size_ondoArray++ ] = x - 1;
                            onGame->arr_undo [ onGame->size_ondoArray++ ] = y - 1;
                        }
                    break;
                }
                else {
                    break;
                }
            }
        j++;
    }
}

void save ( game *onGame )
{
    // the information is stored in the file in the same order as in the writhen struture
    int size;
    int temp;
    char ch;
    ofstream game_save ("Othelo_save.txt");
    // save name of player one
    size = onGame->name_player1.size ();
    for (int i = 0; i < size ; i++)
    {
        game_save.put ( onGame->name_player1[i] );
    }

    game_save.put ('\n');
    // save name of player two
    size = onGame->name_player2.size ();
    for (int i = 0 ; i < size ; i++ )
    {
        game_save.put ( onGame->name_player2[i] );
    }

    game_save.put ('\n');
    // save length of table array
    ch = char ( onGame->len );
    game_save.put (ch);
    game_save.put ('\n');
    // save table array
    for (int i = 0; i < onGame->len ; i++ )
    {
        for (int j = 0; j < onGame->len ; j++)
        {
            ch = char ( onGame->arr[i][j] );
            game_save.put (ch);
        }
    }
    game_save.put ('\n');

    // save turn of player
    if ( onGame->turn ) {
        ch = 'x';
    }else{
        ch = 'o';
    }
    game_save.put (ch);

    game_save.put ('\n');
    // save length of undo array
    ch = char ( onGame->size_ondoArray );
    game_save.put (ch);

    game_save.put ('\n');
    // save undo array
    for (int i = 0; i < onGame->size_ondoArray ; i++ )
    {
        ch = char ( onGame->arr_undo[i] );
        game_save.put (ch);
    }

    game_save.close ();
}

void undo ( game *onGame )
{
    for ( int i = 2 ; i < onGame->size_ondoArray ; i++ ){
        onGame->arr [ onGame->arr_undo[i++] ] [ onGame->arr_undo[i]] = onGame->turn; 
    }
    onGame->arr [ onGame->arr_undo[0]] [ onGame->arr_undo[1]] = -1;
    onGame->turn = !onGame->turn;
}

///////////////////////////////////////////////////////////
void loadGame ()
{
    game *onGame = new game;
    char ch;
    ifstream read_saved ("Othelo_save.txt");
    // name of player one
    read_saved.get (ch);
    do {
        onGame->name_player1 += ch;
        read_saved.get(ch);
    } while (ch != '\n');
    // name of player two
    read_saved.get (ch);
    do {
        onGame->name_player2 += ch;
        read_saved.get(ch);
    } while (ch != '\n');
    // length of array
    read_saved.get (ch);
    onGame->len = int (ch);

    read_saved.get (ch);
 
    onGame->arr = new int *[onGame->len];
    for (int i = 0; i < onGame->len ; i++)
    {
        onGame->arr[i] = new int [onGame->len];
    }

    read_saved.get(ch);
    //array
    for (int i = 0; i < onGame->len; i++ )
    {
        for (int j = 0 ; j < onGame->len; j++ )
        {
            onGame->arr[i][j] = int (ch);
            read_saved.get (ch);
        }
    }

    //read_saved.get (ch);
    // turn 
    read_saved.get (ch);
    if (ch == 'x') {
        onGame->turn = true;
    }else if ( ch == 'o') {
        onGame->turn = false;
    }

    read_saved.get (ch);
    // size of undo array
    read_saved.get (ch);
    onGame->size_ondoArray = int (ch);
    onGame->arr_undo = new int [ onGame->size_ondoArray ];
    // undo array 
    read_saved.get (ch);
    for (int i = 0; i < onGame->size_ondoArray; i++ )
    {
        onGame->arr_undo[i] = int (ch);
        read_saved.get (ch);
    }

    read_saved.close ();
    mainGame ( onGame );
}

//////////////////////////////////////////////////////////
void save_ranking ( string name , int score ) 
{
    ifstream file1 ("Othelo_ranking_save.txt");
    if ( ! file1 ) {
        ofstream file ("Othelo_ranking_save.txt");
        for ( int i = 0 ; i < 100 ; i++ )
        {
            file.put ('%');
        }
        file.close ();
    }
    file1.close();

    ifstream file_ranking ("Othelo_ranking_save.txt");

    string names_list;
    char ch;
    while ( !file_ranking.eof() )
    {
        file_ranking.get (ch);
        names_list += ch;
    }

    bool chang = false;
    int size = name.size ();
    int sizeT = names_list.size();
    int counter = 0;
    for (int i = 0; i < sizeT; i++ )
    {
        if ( names_list [i] == '%' && names_list[i + 1] != '%') {
            int temp = int (names_list[++i]);
            if ( temp == size ) {
                for (int j = 0; j < size ; j++ )
                {
                    if ( names_list[++i] == name[j]) {
                        counter++;
                    }
                }
                if ( counter == size ) {
                    temp = int (names_list[i + 1]);
                    names_list[i + 1] = char ( temp + score );
                    chang = true;
                    break;
                }
            }else {
                i += temp + 1;
            }
        }
    }
    file_ranking.close ();
    remove("Othelo_ranking_save.txt");
    ofstream chang_file ("Othelo_ranking_save.txt");

    if ( !chang ) {
        chang_file.put ('%');
        chang_file.put ( char (size) );
        for (int i = 0; i < size; i++ ) 
        {
            chang_file.put (name[i]);
        }
        chang_file.put ( char ( score ));
        int t = 1;
        for (int i = 0; i < sizeT ; i++ ) 
        {
            if ( names_list[i] == '%') {
                t++;
            }
            if (t == 100){
                break;
            }
            chang_file.put( names_list[i] );
        }
    } else {
        for (int i = 0 ; i < sizeT; i++ )
        {
            chang_file.put (names_list[i]);
        }
    }

    chang_file.close ();
}

void ranking ()
{
    system ("cls");
    for (int i = 0 ; i < 10; i++)
    {
        cout << endl;
    }
    cout << "                                              ************************************" << endl;
    cout << "                                              *      ENTER to see ranking !      *" << endl;
    cout << "                                              ************************************" << endl;
    cin.get();

    ifstream file ("Othelo_ranking_save.txt");

    if ( !file ) {
        system ("cls");
        for (int i = 0 ; i < 20; i++ ) {
            cout << endl;
        }
        cout << "                                           ***********************************************" << endl;
        cout << "                                           *   EROR : You must play at least one time.   *" << endl;
        cout << "                                           ***********************************************" << endl;
        cin.get();
        return;
    }

    string list_ranking;
    char ch;

    while ( !file.eof() )
    {
        file.get(ch);
        list_ranking += ch;
    }

    int size = list_ranking.size ();
    int *rank_array = new int [100];
    for (int i = 0; i < 100; i++ )
    {
        rank_array[i] = 0;
    }
    int t = 0;
    for ( int i = 0 ; i < size - 1  ; i++ )
    {
        if (list_ranking[i + 1] != '%') {
            int temp = int ( list_ranking[++i] );
            i += temp + 1;
            rank_array[ t++ ] = int (list_ranking[ i ]);
        }
    }

    for ( int i = 0; i < 10; i++) 
    {
        for ( int j = 0; j < 99 - i ; j++ )
        {
            if ( rank_array[j] > rank_array[j + 1] )
            {
                int temp = rank_array[j];
                rank_array[j] = rank_array[j +1];
                rank_array[j + 1] = temp;
            }
        }
    }

    int best_score[10];
    int l = 1;
    best_score[0] = rank_array[99];
    for (int i = 1; i < 10; i++ )
    {
        if ( rank_array[98 - i] != rank_array [98 - i + 1]) {
            best_score[l++] = rank_array[98 - i]; 
        }
    }

    string *name_ranking = new string [10];
    int j = 0;
    for ( int bar = 0; bar < t; bar++ )
    {
        for ( int i = 1 ; i < size ; i++  )
        {
            if ( list_ranking[i] != '%' ) {
                int temp = int ( list_ranking[i] );
                if ( list_ranking[i + temp + 1] == rank_array[ 99 - bar] ) {
                    for (int l = 0; l < temp ; l++ ) 
                    {
                        name_ranking[j] += list_ranking[++i];
                    }
                    i++;
                    j++;
                } else {
                    i += temp + 1;
                }
            }
        }
    }

    output_ranking ( name_ranking[0] , rank_array[99] );

    cin.get();
    delete[] rank_array;
    delete[] name_ranking;
}


//////////////////////
void output_mainGame ( game *onGame , int possibleArray[] , int rowC , int colC )
{
    // score of players
    int score_x = 0 , score_o = 0;
    for (int i = 0; i < onGame->len; i++ )
    {
        for (int j = 0 ; j < onGame->len ; j++ )
        {
            if ( onGame->arr[i][j] == 1 ) {
                score_x++;
            }else if ( onGame->arr[i][j] == 0 ) {
                score_o++;
            }
        }
    }

    int n = onGame->len;
	int c ;

    if (n == 8){
        c = 5;
    }else {
        c = 3;
    }
	c-- ;
	
	int spc ;
	if (n == 8)
	{
		spc = 38 ;
	}
	else if ( n == 10)
	{
		spc = 50 ;
	}
	else if (n == 12)
	{
		spc = 45 ;
	}
	else if (n == 14)
	{
		spc = 40 ;
	}
	
	
	int a = 2*c ;
	n = n * c + 1 ;
	int N = n*2 -1 ;
	string tMat[n][N] ;
  	
	for (int i = 0 ; i < n ; i++ )
	{
		for (int j = 0 ; j < (N) ; j++ )
		{
			if (i % c == 0 )
			{
				if ( i == 0)
				{
					if (j == 0)
					{
						tMat[i][i] = "\u250c" ;
					}
					else if (j == (N-1) )
					{
						tMat[i][j] = "\u2510" ;
					}
					else if (j % a == 0 )
					{
						tMat[i][j] = "\u252c" ;
					}
					else
					{
						tMat[i][j] = "\u2500" ;
					}
				}
	
				else if (i == (n-1) )
				{
					if (j == 0)
					{
						tMat[i][j] = "\u2514" ;
					}
					else if (j == (N-1) )
					{
						tMat[n-1][N-1] = "\u2518" ;
					}
					else if (j % a == 0 )
					{
						tMat[i][j] = "\u2534" ;
					}
					else
					{
						tMat[i][j] = "\u2500" ;
					}
				}
				else
				{
					if (j == 0 )
					{
						tMat[i][j] = "\u251c" ;
					}
					else if(j == (N-1) )
					{
						tMat[i][j] = "\u2524" ;
					}
					else if (j % a == 0 )
					{
						tMat[i][j] = "\u253c" ;
					}
					else
					{
						tMat[i][j] = "\u2500" ;
					}
				}
			}
				
			else if (j % a == 0 )
			{
				if ( j == 0 )
				{
					if(i == 0 || i == (n-1))
					{
						continue ;
					}
					else if ( i % c == 0 )
					{
						tMat[i][j] = "\u251c" ;
					}
					else
					{
						tMat[i][j] = "\u2502" ;
					}
				}
				else if (j == (N-1) )
				{
					if (i==0 || i == (n-1) )
					{
						continue ;
					}
					else if( i%c == 0 )
					{	
					tMat[i][j] = "\u2524" ;
					}
					else
					{
						tMat[i][j] = "\u2502" ;
					}
				}
				else
				{
					if(i%c == 0 )
					{
						continue;
					}
					else
					{
						tMat[i][j] = "\u2502" ;
					}
				}
			}
			else
			{		
				if((i+c/2) % c == 0 && (j+a/2) % a == 0 )
				{	
					if (onGame->arr[(i-1)/c][(j-1)/a] == 0 )
					{
						tMat[i][j] = "O" ;
					}
					else if (onGame->arr[(i-1)/c][(j-1)/a] == 1 )
					{
						tMat[i][j] = "X" ;
					}
					else if (onGame->arr[(i-1)/c][(j-1)/a] == (-1) )
					{
						tMat[i][j] = " " ;
					}
				}	
				else
				{
					tMat[i][j] = " " ;
				}
			}
						
		}
	}
	
	int l = possibleArray[0] ;
	int x , y ;
	for (int i = 1 ; i < l ; i+=2 )
	{
		x = possibleArray[i] * c + c/2 ;
		y = possibleArray[i+1] * a + a/2 ; 
        tMat[x][y] = "." ;
	}
	
	
	string b ;
	b = "\u2588" ;

	//cursor

	int i = rowC * c + 1 ;
	int j = colC*a + 1 ;
	int z = i + c - 1 ; //z-c/2 -1
	int s = j + a - 1 ; //s-a/2-1
	//tMat[i + c/2 - 1 ][j + a/2 - 1 ] = "x" ;
	
	for ( int i = rowC * c + 1 ; i < z ; i++ )
	{	
		for (int j = colC * a + 1 ; j < s ; j++)
		{
			if (i == (z-c/2 ) && j == (s-a/2) )
			{
				if ( tMat[i][j] != " " )
				{
					continue ;
				}
				else
				{
					tMat[i][j] = " " ;
				}
			}
			else
			{
				tMat[i][j] = b ;
			}
		}
	}
	
	//Print
		//OTHEllO
		string distance = " ";
    for (int i = 0; i < 98; i++)
    {
        distance += " ";
    }

    //1
	cout << setw(20) ;
    cout << "\u250F";
    for (int i = 0; i < 99; i++)
    {
        cout << "\u2501";
    }
    cout <<  "\u2513" << endl;
    
    //2
	cout << setw(20) ;
    cout << "\u2503" << distance << "\u2503" << endl;

    //3
	cout << setw(20) ;
    cout << "\u2503" << "     " ;
    string space = "  ";
    //output for line 3 ////////////////////////////////////////
        //output o //
    cout << "\u2554";
    for (int i = 0; i < 9; i++)
    {
        cout << "\u2550";
    }
    cout << "\u2557";
        ///////////
    cout << space;

        //output T //
    for (int i = 0; i < 5; i++)
    {
        cout << "\u2550";
    }
    cout << "\u2566";
    for (int i = 0; i < 5; i++)
    {
        cout << "\u2550";
    }
        //////////
    cout << space;

        //output H ///
    cout << "\u2551" << "         " << "\u2551";
        /////////
    cout << space;

        //output E //
    cout << "\u2554";
    for (int i = 0; i < 10; i++){
        cout << "\u2550";
    }
        ////////////
    cout << space;

        //output L //
    cout << "\u2551" << "          ";
        /////////
    cout << space;
        //output L //
    cout << "\u2551" << "          ";
        /////////
    cout << space;

        //output o //
    cout << "\u2554";
    for (int i = 0; i < 9; i++)
    {
        cout << "\u2550";
    }
    cout << "\u2557";
        ///////////
    cout << "     " << "\u2503" << endl;
    //output of line 4 ////////////////////////////////////////////////
	cout << setw(20) ;
    cout << "\u2503" << "     " ;

        //output O
    cout << "\u2551" << "         " << "\u2551";
        ////////
    cout << space;
        //output T
    cout << "     " << "\u2551" << "     ";
        /////////
    cout << space;
        //output H
    cout << "\u2551" << "         " << "\u2551";
        ////////
    cout << space;
        //output E
    cout << "\u2551" << "          ";
        ///////
    cout << space;
        //ouput L
    cout << "\u2551" << "          ";
        //////
    cout << space;
        //ouput L
    cout << "\u2551" << "          ";
        //////
    cout << space;
        //ouput O
    cout << "\u2551" << "         " << "\u2551";
        ////////
    cout << "     " << "\u2503" << endl;

    //ouput of line 5 /////////////////////////////////////////////////
	cout << setw(20) ;
        cout << "\u2503" << "     " ;

        //output O
    cout << "\u2551" << "         " << "\u2551";
        ////////
    cout << space;
        //output T
    cout << "     " << "\u2551" << "     ";
        /////////
    cout << space;
        //ouput H
    cout << "\u2560";
    for (int i = 0; i < 9; i++)
    {
        cout << "\u2550";
    }
    cout << "\u2563";
        /////////
    cout << space;
        //ouput E
    cout << "\u2560";
    for (int i = 0; i < 10; i++)
    {
        cout << "\u2550";
    }
        ////////
    cout << space;
        //ouput L
    cout << "\u2551" << "          ";
        //////
    cout << space;
        //ouput L
    cout << "\u2551" << "          ";
        //////
    cout << space;
            //ouput O
    cout << "\u2551" << "         " << "\u2551";
        ////////
    cout << "     " << "\u2503" << endl;
    
    //output of line 6 /////////////////////////////////////////////
	cout << setw(20) ;
        cout << "\u2503" << "     " ;

            //output O
    cout << "\u2551" << "         " << "\u2551";
        ////////
    cout << space;
        //output T
    cout << "     " << "\u2551" << "     ";
        /////////
    cout << space;
        //output H
    cout << "\u2551" << "         " << "\u2551";
        ////////
    cout << space;
        //output E
    cout << "\u2551" << "          ";
        ///////
    cout << space;
        //ouput L
    cout << "\u2551" << "          ";
        //////
    cout << space;
        //ouput L
    cout << "\u2551" << "          ";
        //////
    cout << space;
        //ouput O
    cout << "\u2551" << "         " << "\u2551";
        ////////
    cout << "     " << "\u2503" << endl;

    //output of line 7 /////////////////////////////////////////////
	cout << setw(20) ;
        cout << "\u2503" << "     " ;

        //output O
    cout << "\u255A";
    for (int i = 0; i < 9; i++)
    {
        cout << "\u2550";
    }
    cout << "\u255D";
        ///////////
    cout << space;
        //output T //
    cout << "     " << "\u2551" << "     ";
        /////////
    cout << space;
        //ouput H ///
    cout << "\u2551" << "         " << "\u2551";
        ////////
    cout << space;
        //ouput E
    cout << "\u255A";
    for (int i = 0; i < 10; i++)
    {
        cout << "\u2550";
    }
        ///////
    cout << space;
        //ouput L
    cout << "\u255A";
    for (int i = 0; i < 10; i++)
    {
        cout << "\u2550";
    }
        ///////
    cout << space;
        //ouput L//
    cout << "\u255A";
    for (int i = 0; i < 10; i++)
    {
        cout << "\u2550";
    }
        /////////
    cout << space;
        //ouput O
    cout << "\u255A";
    for (int i = 0; i < 9; i++)
    {
        cout << "\u2550";
    }
    cout << "\u255D";
        ///////////
    cout << "     " << "\u2503" << endl;

    //8
    //cout << "\u2503" << distance << "\u2503" << endl;

    //9
	cout << setw(20) ;
    cout << "\u255a";
    for (int i = 0; i < 99; i++)
    {
        cout << "\u2501";
    }
    cout <<  "\u255d" << endl;
	
	
	//cout << setw(20) << setw(5) << name_player1 << " : " << "/emtiaz" << setw(10) << name_player2 << " : " << "/emtiaz" << "turn" : 
	for (int i = 0 ; i < n ; i++ )
	{
		cout<< setw(spc) ;
		for (int j = 0 ; j < N ; j++ )
		{
			cout << tMat[i][j] ;
		}
		cout << endl ;
	}
		string line = "\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500\u2500" ;
		bool flag = onGame->turn ;
		string bil = "  " ;
		string bilbilak = "  " ;
		if (flag == true)
		{
			bil = "\u2588 " ;
		}
		else
		{
			bilbilak = "\u2588 " ;
		}

        int size1 = onGame->name_player1.size ();
        if (score_x < 10 ){
            size1++;
        }else if (score_x < 100 ){
            size1 += 2;
        }else{
            size1 += 3;
        }

        int size2 = onGame->name_player2.size ();
        if (score_o < 10 ){
            size2++;
        }else if (score_o < 100 ){
            size2 += 2;
        }else{
            size2 += 3;
        }
	//line 1
	cout << setw(26) << "\u256d" << line << "\u256e" << "                      " <<bil<< onGame->name_player1 << " : " <<  score_x << setw(15 + 14 - size1);
	cout<< "\u256d" <<line << "\u256e" << " " << "\u256d" <<line << "\u256e" << endl ;
	//line 2
	cout << setw(26)  << "\u2502" << "   W(up)  " << "\u2502" <<setw(56) << "\u2502" << " V : Save " << "\u2502" ;
	cout << " " << "\u2502" << " U : Undo " << "\u2502" << endl ;
	//line 3
	cout << setw(15) << "\u256d" << line << "\u253c" << line << "\u253c" << line << "\u256e" << "           "<< bilbilak << onGame->name_player2 <<" : " << score_o ;
	cout << setw(15 + 14 - size2) << "\u2570" << line << "\u256f" ;
	cout << " " << "\u2570" << line << "\u256f" << endl ; 
	//line 4 
	cout << setw(15) << "\u2502" << " A(left)  " << "\u2502" << "  S(down) " << "\u2502" << " D(right) " << "\u2502" ;
	cout << setw(45) << "\u256d" << line << "\u256e" << " " << "\u256d" << line << "\u256e" << endl ;
	//line 5
	cout << setw(15) << "\u2570" << line << "\u2534" << line << "\u2534" << line << "\u256f";
	cout << setw(45) << "\u2502" << " E : Exit " << "\u2502" << " " << "\u2502" <<"Put: Enter" << "\u2502" << endl ;
	//line 5
	cout << setw(91) << "\u2570" << line << "\u256f" << " " << "\u2570" << line << "\u256f" << endl ;
}


void hard_game ()
{
	string space ;
	for (int i = 0 ; i < 30 ; i++ )
	{
		space += " " ;
	}
    game *onGame = new game;
    // out put for giving turn and length of array
	cout << setw(40) << "*** X or O or r(random) ? *** " << endl ;
    char ch;
    cin >> ch;

    bool your_turn;
    if ( ch == 'x' ) 
	{
        your_turn = true;
    }
	else if( ch == 'o' )
		{
        your_turn = false;
    }
    else if ( ch == 'r' ) {
        for (int i = 0 ; i <= 1 ; i++)
        {
            your_turn = rand();
        }
    }

    // input legth of our array 
	cout << setw(40) << " Please Enter dimension : " << endl ;
	cout << setw(40) << "Dimension : " ;
    cin >> onGame->len;
	if ((onGame->len < 8 ) || ( onGame->len > 14 ) || ( onGame->len % 2 != 0 ))
	{
  		int curser = 0 ; 
		char key ;
		bool ans = true;
        while ( ans ) 
        {
			system ("cls") ;
			cout << space << "          " << "\u2588" " Invalid length! " << endl ;
			cout << space << "          " << "Please Choose what to do next : " << endl ;
			output_wrong_len(curser) ;
			key = getch ();
            switch (key)
            {
            case '\r':
                ans = false;
                break;
            case 'a':
                if (curser == 0){
                    curser = 1;
                }else{
                    curser--;
                }
                break;
            case 'd':
                if (curser == 1){
                    curser = 0;
            }
			else
			{  
                    curser++;
                }
                break;
            }
		}
		if (curser == 0)
		{
			cout << endl << setw(43) << "\u2588" << " Dimension : " ;
			cin >> onGame->len;
				 if ((onGame->len < 8 ) || ( onGame->len > 14 ) || ( onGame->len % 2 != 0 ))
				 {
					 return ;
				 }
		}
        else if (onGame->len == -1)
		{
            return;
        } else {
        	return;
		}
	}
			
	onGame->name_player1 = "you" ;
	onGame->name_player2 = "cpu" ;
    ///// making array of game
    onGame->arr = new int *[onGame->len];
    for (int i = 0; i < onGame->len; i++ )
    {
        onGame->arr[i] = new int [onGame->len];
    }

    for (int i = 0; i < onGame->len; i++)
    {
        for (int j = 0; j < onGame->len; j++)
        {
            onGame->arr[i][j] = -1;
        }
    }

    //put x and o in first of the game
    int x = onGame->len / 2 - 1;
    onGame->arr[x][x] = 1;
    onGame->arr[x + 1][x + 1] = 1;
    onGame->arr[x][x + 1] = 0;
    onGame->arr[x + 1][x] = 0;

    onGame->arr_undo = new int [ onGame->len * onGame->len ];

    while (true) 
    {
        if ( your_turn == onGame->turn ) 
        {
            bool end;
            move ( onGame , end );
            if ( end ){
                break;
            }
            
        }else
        {
            int *arr_posibleTable = new int  [onGame->len * onGame->len];
            check_for_posibleTable ( onGame , arr_posibleTable );
            int size = arr_posibleTable[0];
            if (size == 1) {
                onGame->turn = !onGame->turn;
                check_for_posibleTable ( onGame , arr_posibleTable );
                size = arr_posibleTable[0];
                if ( size == 1 ){
                    break;
                }else{
                    continue;
                }
            }
            // this block is for finding the least choice 
            int l = 1;
            int row = arr_posibleTable[ l ];
            int col = arr_posibleTable[ ++l ];
            put ( onGame , row , col , arr_posibleTable , size );
            int *arr1 = new int [ onGame->len * onGame-> len];
            check_for_posibleTable ( onGame , arr1 );
            int len1 = arr1[0];
            undo ( onGame );
            int min = len1;
            int bestx = row;
            int besty = col; 
            delete[] arr1;
            for ( int i = 1 ; i < (size - 1) /2; i++ )
            {
                row = arr_posibleTable[ ++l ];
                col = arr_posibleTable[ ++l ];
                put ( onGame , row , col , arr_posibleTable , size );
                int *arr1 = new int [ onGame->len * onGame-> len];
                check_for_posibleTable ( onGame , arr1 );
                len1 = arr1[0];
                undo ( onGame );
                if ( len1 < min ) {
                    min = len1;
                    bestx = row;
                    besty = col;
                }
                delete[] arr1;
            }

            put (onGame , bestx , besty , arr_posibleTable , size); 
        }
    }
	output_endGame ( onGame );
}

void move ( game *onGame , bool &end )
{
    char key;
        int *arr_posibleTable = new int  [onGame->len * onGame->len];
        check_for_posibleTable ( onGame , arr_posibleTable );
        int size = arr_posibleTable[0];
        system ("cls");
        output_mainGame ( onGame , arr_posibleTable , onGame->row , onGame->col);            
        if (size == 1) {
            onGame->turn = !onGame->turn;
            check_for_posibleTable ( onGame , arr_posibleTable );
            size = arr_posibleTable[0];
            if ( size == 1 ){
                end = true;
            }
        }
        key = getch ();
        switch (key)
        {
        case '\r':
            put ( onGame , onGame->row , onGame->col , arr_posibleTable , size );
            break;
        case 'w':
            if (onGame->row == 0){
            onGame->row = onGame->len - 1;
            }else{
                onGame->row--;
            }
            break;
        case 's':
            if (onGame->row == onGame->len - 1){
                onGame->row = 0;
            }else{
                onGame->row++;
            }
            break;
        case 'a' :
            if (onGame->col == 0){
                onGame->col = onGame->len - 1;
            }else{
                onGame->col--;
            }
            break;
        case 'd' :
            if (onGame->col == onGame->len - 1){
                onGame->col = 0;
            }else{
                onGame->col++;
            }
            break;
        case 'e' :
            end = true;
            return ;
            break;
        }
        delete[] arr_posibleTable;
}

void easy_game ()
{
   string space ;
	for (int i = 0 ; i < 30 ; i++ )
	{
		space += " " ;
	}
    game *onGame = new game;
    // out put for giving turn and length of array
	cout << setw(40) << "*** X or O or r(random) ? *** " << endl ;
    char ch;
    cin >> ch;

    bool your_turn;
    if ( ch == 'x' ) 
	{
        your_turn = true;
    }
	else if( ch == 'o' )
		{
        your_turn = false;
    }
    else if ( ch == 'r' ) {
        for (int i = 0 ; i <= 1 ; i++)
        {
            your_turn = rand();
        }
    }

    // input legth of our array 
	cout << setw(40) << " Please Enter dimension : " << endl ;
	cout << setw(40) << "Dimension : " ;
    cin >> onGame->len;
	if ((onGame->len < 8 ) || ( onGame->len > 14 ) || ( onGame->len % 2 != 0 )) 
	{
  		int curser = 0 ; 
		char key ;
		bool ans = true;
        while ( ans ) 
        {
			system ("cls") ;
			cout << space << "          " << "\u2588" " Invalid length! " << endl ;
			cout << space << "          " << "Please Choose what to do next : " << endl ;
			output_wrong_len(curser) ;
			key = getch ();
            switch (key)
            {
            case '\r':
                ans = false;
                break;
            case 'a':
                if (curser == 0){
                    curser = 1;
                }else{
                    curser--;
                }
                break;
            case 'd':
                if (curser == 1){
                    curser = 0;
            }
			else
			{  
                    curser++;
                }
                break;
            }
		}
		if (curser == 0)
		{
			cout << endl << setw(43) << "\u2588" << " Dimension : " ;
			cin >> onGame->len;
				 if ((onGame->len < 8 ) || ( onGame->len > 14 ) || ( onGame->len % 2 != 0 ))
				 {
					 return ;
				 }
			}
            else if (onGame->len == -1)
			{
                return;
            }
			else 
			{
            return;
			}
	}
			
	onGame->name_player1 = "you" ;
	onGame->name_player2 = "cpu" ;

    ///// making array of game 
    onGame->arr = new int *[onGame->len];
    for (int i = 0; i < onGame->len; i++ )
    {
        onGame->arr[i] = new int [onGame->len];
    }

    for (int i = 0; i < onGame->len; i++)
    {
        for (int j = 0; j < onGame->len; j++)
        {
            onGame->arr[i][j] = -1;
        }
    }

    //put x and o in first of the game
    int x = onGame->len / 2 - 1;
    onGame->arr[x][x] = 1;
    onGame->arr[x + 1][x + 1] = 1;
    onGame->arr[x][x + 1] = 0;
    onGame->arr[x + 1][x] = 0;

	onGame->arr_undo = new int  [onGame->len * onGame->len];
    
    while (true) 
    {
        if ( your_turn == onGame->turn ) 
        {
            bool end;
            move ( onGame , end );
            if (end) {
                break;
            }
        }else
        {
            int *arr_posibleTable = new int  [onGame->len * onGame->len];
            check_for_posibleTable ( onGame , arr_posibleTable );
            int size = arr_posibleTable[0];
            if (size == 1) {
                onGame->turn = !onGame->turn;
                check_for_posibleTable ( onGame , arr_posibleTable );
                size = arr_posibleTable[0];
                if ( size == 1 ){
                    break;
                }else{
                    continue;
                }
            }

            int l = 1;
            int row = arr_posibleTable[ l ];
            int col = arr_posibleTable[ ++l ];
            put ( onGame , row , col , arr_posibleTable , size );
            int *arr1 = new int [ onGame->len * onGame-> len];
            check_for_posibleTable ( onGame , arr1 );
            int len1 = arr1[0];
            undo ( onGame );
            int max = len1;
            int bestx = row;
            int besty = col; 
            delete[] arr1;
            for ( int i = 1 ; i < (size - 1) /2; i++ )
            {
                row = arr_posibleTable[ ++l ];
                col = arr_posibleTable[ ++l ];
                put ( onGame , row , col , arr_posibleTable , size );
                int *arr1 = new int [ onGame->len * onGame-> len];
                check_for_posibleTable ( onGame , arr1 );
                len1 = arr1[0];
                undo ( onGame );
                if ( len1 > max ) {
                    max = len1;
                    bestx = row;
                    besty = col;
                }
                delete[] arr1;
            }

            put (onGame , bestx , besty , arr_posibleTable , size); 
			delete[] arr_posibleTable;
        }
    }
	output_endGame ( onGame );
}

void single_player ()
{
    int curser = 0;
    char key; 
    while (true) 
    {
        system ("cls");
        output_singlePlayer (curser);
        key = getch ();
        switch (key)
        {
        case '\r':
            if (curser == 0) {
                easy_game();
            }else{
                hard_game();
            }
            return;
            break;
        case 'a':
            if (curser == 0){
                curser = 1;
            }else{
                curser--;
            }
            break;
        case 'd':
            if (curser == 1){
                curser = 0;
            }else{
                curser++;
            }
            break;
        }
    }
}

int eror_wrong_length ()
{
    // out put 
    int ans;
    cin >> ans;
    if ((ans < 8 ) || ( ans > 14 ) || ( ans % 2 != 0 )){
        //output_of_eror_for_invalid_len ;
        char a;
        cin >> a;
        if (a == '1'){
            return eror_wrong_length ();
        }else
        if (a == '2'){
            return 0;
        }
    }
    return ans;
}

void output_singlePlayer( int curser )
{
    cout << endl << endl << endl << endl << endl << endl << endl  << endl << endl << endl;
	cout << setw(70) ;
	cout << "Please Enter the level : Easy or Hard ? " << endl << endl ;

	string level[3][15] ;
	for (int j = 0 ; j < 15 ; j++ )
	{
		for (int i = 0 ; i < 3 ; i++ )
		{
			level[i][j] = " " ;
		} 
	}
	for (int j = 1 ; j < 14 ; j++ )
	{
		level[0][j] = "\u2500" ;
		level[2][j] = "\u2500" ;
	}
	
	// Easy
	 level[1][2] = "E" ; level[1][3] = "a" ; level[1][4] = "s" ; level[1][5] = "y" ;
	// Hard
	 level[1][9] = "H" ; level[1][10] = "a" ; level[1][11] = "r" ; level[1][12] = "d" ;

	
	
			level[0][0] = "\u256d" ;
			level[0][14] = "\u256e" ;
			level[2][0] = "\u2570" ;
			level[2][14] = "\u256f" ;
			
			level[0][7] = "\u252c" ;
			level[2][7] = "\u2534" ;
		
			level[1][0] = "\u2502" ;
			level[1][7] = "\u2502" ;
			level[1][14]= "\u2502" ;

		
	// Cursor
	if (curser == 0 )
	{
		level[1][1] = "\u2588" ;
	}
	else
	{
		level[1][8] = "\u2588" ;
	}
	
	// prints
	for (int i = 0 ; i < 3 ; i++ )
	{
		cout << setw(45) ;
		for (int j = 0 ; j < 15 ; j++ )
		{
			cout << level[i][j] ; 
		}
		cout << endl ;
	}	
}

void output_newGame ( int cursor )
{
    cout << endl << endl << endl << endl << endl << endl << endl  << endl << endl << endl;
	cout << setw(70) ;
	cout << "Single or Double Player ? " << endl << endl ;
	
	string gameType[5][17] ;
	for (int i = 0 ; i<5 ; i++ )
	{
		for (int j = 0 ; j < 17 ; j++ )
		{
			gameType[i][j] = " " ;
		} 
	}
	for (int j = 1 ; j < 16 ; j++ )
	{
		gameType[0][j] = "\u2500" ;
		gameType[2][j] = "\u2500" ;
		gameType[4][j] = "\u2500" ;
	}
	
	// single player
	 gameType[1][2] = "S" ; gameType[1][3] = "i" ; gameType[1][4] = "n" ; gameType[1][5] = "g" ; gameType[1][6] = "l" ;gameType[1][7] = "e" ;
	gameType[1][9] = "P" ; gameType[1][10] = "l" ; gameType[1][11] = "a" ; gameType[1][12] = "y" ; gameType[1][13] = "e" ; gameType[1][14] = "r" ;
	// double player
	 gameType[3][2] = "D" ; gameType[3][3] = "o" ; gameType[3][4] = "u" ; gameType[3][5] = "b" ; gameType[3][6] = "l" ;gameType[3][7] = "e" ;
	gameType[3][9] = "P" ; gameType[3][10] = "l" ; gameType[3][11] = "a" ; gameType[3][12] = "y" ; gameType[3][13] = "e" ; gameType[3][14] = "r" ;
	
	
			gameType[0][0] = "\u256d" ;
			gameType[0][16] = "\u256e" ;
			gameType[4][0] = "\u2570" ;
			gameType[4][16] = "\u256f" ;
			gameType[2][0] = "\u251c" ;
			gameType[2][16] = "\u2524" ;
		
			gameType[1][0] = "\u2502" ;
			gameType[1][16] = "\u2502" ;
			gameType[3][0] = "\u2502" ;
			gameType[3][16] = "\u2502" ;
		
	// Cursor
	if (cursor == 0 )
	{
		gameType[1][1] = "\u2588" ;
	}
	else
	{
		gameType[3][1] = "\u2588" ;
	}
	
	// prints
	for (int i = 0 ; i < 5 ; i++ )
	{
		cout << setw(50) ;
		for (int j = 0 ; j < 17 ; j++ )
		{
			cout << gameType[i][j] ; 
		}
		cout << endl ;
	}	
}

void output_ranking ( string name , int point )
{
	for (int i = 0 ; i < 5  ; i++ )
	{
		cout << endl ;
	}
	//line 1 
	cout << setw(55) ;
	for (int i = 0 ; i < 15 ; i++ )
	{
		cout << "*";
	}
	cout << endl ;
	//line 2
	cout << setw(55) ;
	cout << "*" << "   Ranking   " << "*" << endl ;
	// line 3 
	cout << setw(55) ;
	cout << "*" << setw(14) << "*" << endl ;
	cout << setw(55) ;
	for (int i = 0 ; i < 15 ; i++ )
	{
		cout << "*";
	}
	cout << endl << endl ;
	//line 4 
	cout << "                                                     And the Best Player is " << endl << endl << setw(65) << name << endl << endl << setw(65) << " with " << endl << endl << setw(60) << point << " points ! " << endl ;
	cout << endl << endl ;
	
	string line ;
	for (int i = 0 ; i < 16 ; i++ )
	{
		line+= "\u2500" ;
	}
	//
	cout << setw(57) << "\u256d" << line << "\u256e" << endl ;
	//
	cout << setw(57) << "\u2502" << "Congratulations!" << "\u2502" << endl ;
	//
	cout << setw(57) << "\u2570" << line << "\u256f" << endl ;
}

void output_wrong_len ( int cursor)
{
	cout << setw(70) ;
	cout << endl << endl ;
	string error1[3][15] ;
	for (int j = 0 ; j < 15 ; j++ )
	{
		for (int i = 0 ; i < 3 ; i++ )
		{
			error1[i][j] = " " ;
		} 
	}
	for (int j = 1 ; j < 14 ; j++ )
	{
		error1[0][j] = "\u2500" ;
		error1[2][j] = "\u2500" ;
	}
	
	// try
	 error1[1][2] = "T" ; error1[1][3] = "r" ;error1[1][4] = "y" ;
	// menu
	 error1[1][9] = "M" ; error1[1][10] = "e" ; error1[1][11] = "n" ; error1[1][12] = "u" ;

	
	
			error1[0][0] = "\u256d" ;
			error1[0][14] = "\u256e" ;
			error1[2][0] = "\u2570" ;
			error1[2][14] = "\u256f" ;
			
			error1[0][7] = "\u252c" ;
			error1[2][7] = "\u2534" ;
		
			error1[1][0] = "\u2502" ;
			error1[1][7] = "\u2502" ;
			error1[1][14]= "\u2502" ;

		
	// Cursor
	if (cursor == 0 )
	{
		error1[1][1] = "\u2588" ;
	}
	else
	{
		error1[1][8] = "\u2588" ;
	}
	
	// prints
	for (int i = 0 ; i < 3 ; i++ )
	{
		cout << setw(45) ;
		for (int j = 0 ; j < 15 ; j++ )
		{
			cout << error1[i][j] ; 
		}
		cout << endl ;
	}
}

void output_endGame ( game *onGame )
{
	//output end of the game
    int p = 0;
    int o = 0;
    for ( int i = 0; i < onGame->len; i++ )
    {
        for (int j = 0; j < onGame->len ; j++ )
        {
            if (onGame->arr[i][j] == 1) {
                p++;
            }else if (onGame->arr[i][j] == 0) {
                o++;
            }
        }
    }
	char mohre;
    if (p > o){
		mohre = 'x';
		cin.get ();
    }else if (o >  p){
		mohre = 'o';
		cin.get ();
    }else{
		cout << "                                                      ******************************" << endl;
        cout << "                                                      *       you are equal !      *" << endl;
		cout << "                                                      ******************************" << endl;
        cin.get();
    }
	system("cls") ;
	string line ;
	for (int i = 0 ; i < 20 ; i++ )
	{
		cout << endl ;
	}
	// output 
	for (int i = 0 ; i < 16 ; i++ )
	{
		line+= "\u2500" ;
	}
		cout << setw(32) << "\u256d" << line << "\u256e" << endl ;
	//
	cout << setw(32) << "\u2502" << "Congratulations!" << "\u2502" << endl ;
	//
	cout << setw(32) << "\u2570" << line << "\u256f" << endl ;
	cout << setw(36) << mohre << " won! " << endl ;
	cin.get ();
}

